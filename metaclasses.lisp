(in-package :defplayer.disk-backing)

(defclass disk-database (pcl::standard-class)
  ())

(defmethod pcl:validate-superclass ((class disk-database) (new-super pcl::standard-class))
  t)

(defmethod compute-slots ((class disk-database))
  "Add the slot for storing the root pathname of the database."
  (list* (make-instance 'pcl:standard-effective-slot-definition
                        :name 'root-pathname
                        :initargs '(root-pathname)
                        :readers '(root-pathname)
                        :initform '(error "A disk-backed database must be initialized with\
                                           the root directory of its disk contents.")
                        :initfunction (lambda ()
                                        (error "A disk-backed database must be initialized with\
                                                the root directory of its disk contents."))
                        :documentation "The location on disk where the disk-backed database's\
                                        contents are rooted.")
         (make-instance 'pcl:standard-effective-slot-definition
                        :name 'directory-attribute-file-name
                        :initargs '(directory-attribute-file-name)
                        :readers '(directory-attribute-file-name)
                        :initform ".attributes"
                        :initfunction (lambda () ".attributes")
                        :documentation "The file name used for storing a DIRECTORY-BACKED-INSTANCE's\
                                        attributes")
         (make-instance 'pcl:standard-effective-slot-definition
                        :name 'file-attribute-file-name
                        :initargs '(file-attribute-file-name)
                        :readers '(file-attribute-file-name)
                        :initform ".attributes.~A"
                        :initfunction (lambda () ".attributes.~A")
                        :documentation "A format string to construct the file name used for storing a\
                                        FILE-BACKED-INSTANCE's attributes, given the NAME of the\
                                        instance")
         (call-next-method)))

(defclass disk-backed-class (pcl::standard-class)
  ((direct-containers :initarg contained-by
                      :initform nil
                      :accessor direct-containers)
   (effective-containers :accessor effective-containers)
   (disk-name :initarg  disk-name
              :accessor disk-name)))

(defmethod pcl:validate-superclass ((class disk-backed-class) (new-super pcl::standard-class))
  t)

(defmethod compute-slots ((class disk-backed-class))
  "Add the slot for storing the root pathname of the database."
  (cons (make-instance 'pcl:standard-effective-slot-definition
		       :name 'root-pathname
		       :initargs '(root-pathname)
		       :readers '(root-pathname)
		       :initform '(error "A disk-backed database must be initialized with\
                                          the root directory of its disk contents.")
		       :initfunction (lambda ()
				       (error "A disk-backed database must be initialized with\
                                               the root directory of its disk contents."))
		       :documentation "The location on disk where the disk-backed database's\
                                       contents are rooted.")
	(call-next-method)))

(defmethod direct-containers ((class pcl::standard-class))
  nil)

(define-condition illegal-container-spec (program-error)
  ((class :initarg :class
          :reader illegal-container-spec-class))
  (:report (lambda (condition stream)
             (format stream "The class ~A's container spec is illegal: ~A"
                     (illegal-container-spec-class condition)
                     (illegal-container-spec-reason condition)))))

(define-condition override-many-container-spec-to-single (illegal-container-spec)
  ())

;;; !!!!!!!!!!!!!!!!!!!!!! need to make specification fully boolean??
;;; e.g., (any artist album) - one artist, one album, or one of each
;;;       (any artist* album) - one artist, many artists, one album, one artist and one album, or many artists and one album
;;;       (any artist* album*) - ...
;;;       (one artist* album*) - one artist, many artists, one album, or many albums
;;;       (one artist album) - one artist or one album
;;;       (all artist album) - one artist and one album
;;;       (all artist* album*) - one or many artists and one or many albums
;;; 
;;; SO, we have a total of 3 n-ary operators:
;;;  1. plural          - :multi
;;;  2. single choice   - :one
;;;  3. multiple choice - :any
;;;  4. multiple        - :all
;;;
;;; all are associative and commutative
;;;
;;; ( or  x x)  = x
;;; (:one x x)  = x
;;;
;;; ( and x x)  = x
;;; (:any x x) /= x
;;;
;;; ( or  ( and x y...) ( and x z...) ...)  = ( and x ( or  ( and y...) ( and z...) ...))
;;; ( +   ( *   x y...) ( *   x z...) ...)  = ( *   x ( +   ( *   y...) ( *   z...) ...))
;;; (:one (:any x y...) (:any x z...) ...)  = (:any x (:one (:any y...) (:any z...) ...))
;;;
;;; ( *   ( +   x1 x2 ...) ( +   y1 y2...))  = ( +   ( *   x1 y1) ( *   x1 y2) ... ( *   x2 y1) ( *   x2 y2) ...)
;;; (:any (:one x1 x2 ...) (:one y1 y2...))  = (:one (:any x1 y1) (:any x1 y2) ... (:any x2 y1) (:any x2 y2) ...)
;;;
;;; (:multi x y ...) = (:any (:multi x) (:multi y) ...)
;;;
;;; (:multi (:foo x...) ...) = (:multi x... ...)

(defmethod illegal-container-spec-reason ((error override-many-container-spec-to-single))
  "Attempt to override the inherited specification that it can be contained by many ~As to one that limits to a single ~A")

(defun merge-container-specs (all-containers additional-containers class)
  (dolist (new-spec additional-containers)
    (multiple-value-bind (container type)
        (cond
         ((symbolp new-spec)
          (values new-spec t))
         ((and (consp new-spec) (eq :many (first new-spec)))
          (values cons (second new-spec) :many))))
    (let ((existing-spec (assoc container all-containers)))
      (cond
       ((null existing-spec)
        (push (cons container type) all-containers))
       ((and (eq (cdr existing-spec) type))
        nil)
       ((eq type :many)
        (setf (cdr existing-spec) :many))
       ((eq type t)
        (cerror (format nil "Leave the containment specification for ~A as inherited: potentially belonging to many ~As."
                        class (car existing-spec))
                'illegal-container-spec
               :class class
               :type :override-many-to-single))
       (t (error 'unknown-container-spec :class class)))))
  all-containers)

(defmethod finalize-inheritance :after ((class disk-backed-class))
  (let ((effective-containers nil))
    (dolist (superclass (pcl:class-precedence-list class))
      (setf effective-containers
            (merge-container-specs effective-containers
                                   (direct-containers superclass))))
    (setf (effective-containers class) effective-containers)))

(defclass directory-backed-class (disk-backed-class)
  ())

(defclass file-backed-class (disk-backed-class)
  ())

(defgeneric legal-container-type-p (object directory))

(defmethod legal-container-type-p ((object disk-backed-class) (directory directory-backed-class))
  (dolist (container-spec (effective-containers object) nil)
    (when (eq (car container-spec) (class-name directory))
      (return t))))

(defmethod legal-container-type-p ((object disk-backed-class) (directory file-backed-class))
  nil)

(defun add-to-container (object container)
  "Adds OBJECT to CONTAINER, removing it from CONTAINER's parents, if necessary, and signalling
errors if this operation would result in an inconsistency."
  (if (legal-container-type-p (pcl:class-of object) (pcl:class-of directory))
      (progn ;; TODO: make "atomic"
        (remove-from-container object ) ;; how to make sure that the object gets removed from all parents?
        (add-to-container-using-class object container))
    (error 'illegal-container-type :object object :container container))))

;;; TODO: putting a disk-backed-instance into a directory-backed-instance

