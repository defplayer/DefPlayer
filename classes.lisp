(in-package :defplayer.disk-backing)

(defclass music-archive ()
  ()
  (:metaclass disk-database))

(defclass artist ()
  ((name :initarg  name
         :accessor name
	 :type string))
  (:metaclass directory-backed-class)
  (contained-by nil)
  (disk-name :multi name))

(defclass album ()
  ((name :initarg  name
         :accessor name
	 :type string)
   (year :initarg  year
         :accessor year
	 :type unsigned-byte))
  (:metaclass directory-backed-class)
  (contained-by (:one nil artist))
  (disk-name :multi name))

(defclass movie-soundtrack (album)
  ()
  (:metaclass directory-backed-class))

(defclass multi-volume-album (album)
  ()
  (:metaclass directory-backed-class))

(defclass album-volume ()
  ((volume :initarg  volume
	   :accessor volume
	   :type (or rational string)))
  (:metaclass directory-backed-class)
  (contained-by multi-volume-album)
  (disk-name volume))

(defclass song ()
  ((name :initarg :name
         :accessor name
	 :type string))
  (:metaclass directory-backed-class)
  (contained-by nil)
  (disk-name :multi name))

(defclass performance ()
  ((year :initarg  year
         :accessor year
	 :type unsigned-byte)
   (duration :initarg  duration
             :accessor duration
	     :type unsigned-byte
	     :documentation "Duration in seconds"))
  (:metaclass directory-backed-class)
  (contained-by (:all (:multi artist album) song)))

(defclass remix (performance)
  ((remix-name :initarg  remix-name
	       :accessor remix-name
	       :type string))
  (:metaclass directory-backed-class)
  (disk-name remix-name)
  (contained-by :any (:multi song)))

(defclass concert (album)
  ((venue :initarg :venue
          :accessor venue
          :type string))
  (:metaclass directory-backed-class)
  (disk-name (venue date)))

(defclass live-performance (performance)
  ()
  (:metaclass directory-backed-class)
  (contained-by :all concert))

(defclass live-remix (live-performance remix)
  ()
  (:metaclass directory-backed-class)
  (contained-by :all))

(defclass encoding ()
  ((filename :initarg  filename
	     :accessor filename
	     :type string)
   (format :initarg :format
	   :accessor encoding-format
	   :type audio-format)
   (compression-strength :initarg :compression-strength
			 :accessor compression-strength
			 :documentation "Interpretation depends on format")
   (defects :initarg :defects
            :accessor defects))
  (:metaclass file-backed-class)
  (contained-by performance)
  (disk-name :multi filename))

(defclass audio-format ()
  ((name :initarg  name
         :accessor name
         :type string
         :documentation "Human-readable name for the format, e.g. \"MPEG layer 3 audio track\"")
   (extensions :initarg  extensions
               :accessor extensions
               :type list
               :documentation "List of commonly-found extensions, with the preferred one first.")
   (compression-type :initarg  compression-type
                     :accessor compression-type
                     :type (member nil :lossy :lossless)
                     :documentation "NIL for uncompressed, :LOSSY for lossy compression, :LOSSLESS for lossless compression")
   (compression-strength-description :initarg  compression-strength-description
                                     :accessor compression-strength-description
                                     :type string
                                     :documentation "Human-readable description for the units of an encoding's ``compression-strength'' slot value; leave unbound if format does not support compression or compression is not controllable")))

(defvar *audio-formats*
  (list (make-instance 'audio-format
                       'name "MPEG layer 3 audio track"
                       'extensions '("mp3")
                       'compression-type :lossy
                       'compression-strength-description "kilobits per second")
        (make-instance 'audio-format
                       'name "OGG Vorbis audio track"
                       'extensions '("ogg")
                       'compression-type :lossy
                       'compression-strength-description "kilobits per second")
        (make-instance 'audio-format
                       'name "WAV file"
                       'extensions '("wav")
                       'compression-type NIL)
        (make-instance 'audio-format
                       'name "FLAC audio track"
                       'extensions '("flac")
                       'compression-type :lossless)))
